package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var (
	port      int
	directory string
	echo      bool
)

func init() {
	flag.IntVar(&port, "p", 8080, "port")
	flag.StringVar(&directory, "d", ".", "the directory of static file to host")
	flag.BoolVar(&echo, "e", false, "is echo server")
	flag.Parse()
}

func Log(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func Echo(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s\n", r.URL.String())
	log.Printf("headers: %+v", r.Header)
	bd, err := ioutil.ReadAll(r.Body)
	log.Printf("body: %+v, %+v", string(bd), err)
	w.Write([]byte("ok"))
}

func EchoGin(context *gin.Context) {
	r := context.Request
	w := context.Writer
	log.Printf("%s\n", r.URL.String())
	log.Printf("headers: %+v", r.Header)
	bd, err := ioutil.ReadAll(r.Body)
	log.Printf("body: %+v, %+v", string(bd), err)
	w.Write([]byte("ok"))
}

func EchoStream(context *gin.Context) {
	countStr := context.Param("count")
	log.Printf("count is %s", countStr)
	count, _ := strconv.Atoi(countStr)

	context.Stream(func(w io.Writer) bool {
		count--
		w.Write([]byte(fmt.Sprintf("%d", count)))
		<-time.After(time.Second)
		return count > 0
	})
}

func main() {
	if echo {
		g := gin.New()
		g.NoRoute(EchoGin)

		g.Any("/stream/:count", EchoStream)

		// http.HandleFunc("/", Echo)
		// http.HandleFunc("/stream/:count", func(writer http.ResponseWriter, request *http.Request) {
		//	writer.Header().Set("Content-Type", "text/event-stream")
		//	writer.Header().Set("Cache-Control", "no-cache")
		//	writer.Header().Set("Connection", "keep-alive")
		//
		//})

		log.Printf("Serving on HTTP port: %d\n", port)
		log.Fatal(g.Run(fmt.Sprintf(":%d", port)))
		// log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), http.DefaultServeMux))
	} else {

		var frontend fs.FS = os.DirFS(directory)
		httpFS := http.FS(frontend)
		fileServer := http.FileServer(httpFS)
		serveIndex := ServeFileContents("index.html", httpFS)

		http.Handle("/", intercept404(fileServer, serveIndex))

		// http.Handle("/", http.FileServer(http.Dir(directory)))

		log.Printf("Serving %s on HTTP port: %d\n", directory, port)
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), Log(http.DefaultServeMux)))
	}
}
